//
// Created by debianstnin on 08/10/18.
//

#ifndef NOTEPADSERVIDOR_PROTOCOLO_H
#define NOTEPADSERVIDOR_PROTOCOLO_H

#include "../json_include/json.hpp"
#include "../Cliente/InterfaceMensagem.h"
#include "EnumeracaoProtocolos.h"
#include "../Cliente/Mensagem.h"

class Protocolo {

public:

    InterfaceMensagem * criarProtocoloToken();

};


#endif //NOTEPADSERVIDOR_PROTOCOLO_H
