//
// Created by debianstnin on 08/10/18.
//

#ifndef NOTEPADSERVIDOR_PROTOCOLOCOMUNICATIVO_H
#define NOTEPADSERVIDOR_PROTOCOLOCOMUNICATIVO_H

#include "../json_include/json.hpp"
#include "../Cliente/InterfaceMensagem.h"
#include "../Servidor/Token.h"

#include <iostream>
#include <string>
#include <sstream>

class ProtocoloComunicativo {

public:

    explicit ProtocoloComunicativo( InterfaceMensagem * mensagem );

    bool mensagemValida();

    std::string obterMensagemFormatoString();

    bool validarToken( InterfaceMensagem * mensagem );

private:

    InterfaceMensagem * camadaMensagem;
    std::string mensagemResposta;

    bool valido;

    Token token;


};


#endif //NOTEPADSERVIDOR_PROTOCOLOCOMUNICATIVO_H
