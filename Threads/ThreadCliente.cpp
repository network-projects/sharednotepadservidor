//
// Created by debianstnin on 07/10/18.
//

#include "ThreadCliente.h"

ThreadCliente::ThreadCliente( InterfaceClienteComunicacao * cliente, InterfaceListaAuditorio * auditorio ) {

    this->cliente = cliente;
    this->auditorio = auditorio;

    this->lista = auditorio->obterListaNomeClientesString();

}

ThreadCliente::~ThreadCliente() {

    auditorio->remover( cliente );

    delete cliente;

}

void ThreadCliente::definirTipoCliente() {

    auto nome = cliente->receberMensagem(); // receber nome e token
    cliente->definirNomeCliente( nome );

    auto token = cliente->receberMensagem();

    if( cliente->mensagemRecebidaSucesso() ) {

        ProtocoloComunicativo protocoloComunicativo(token);

        if( protocoloComunicativo.validarToken(token) ){

            clienteOrador();

        }else {

            clienteComum();

        }

    }


}

void ThreadCliente::clienteOrador() {

    while( cliente->clienteConectado() ){



    }

}

void ThreadCliente::clienteComum() {

}