//
// Created by debianstnin on 07/10/18.
//

#ifndef NOTEPADSERVIDOR_THREADCLIENTE_H
#define NOTEPADSERVIDOR_THREADCLIENTE_H

#include <thread>

#include "../Cliente/InterfaceClienteComunicacao.h"
#include "../Auditorio/InterfaceListaAuditorio.h"
#include "../Protocolos/Protocolo.h"
#include "../Protocolos/ProtocoloComunicativo.h"

class ThreadCliente{

public:

    explicit ThreadCliente( InterfaceClienteComunicacao * cliente, InterfaceListaAuditorio * auditorio );
    ~ThreadCliente();

    void definirTipoCliente();
    void clienteOrador();
    void clienteComum();

private:

    InterfaceClienteComunicacao * cliente;
    InterfaceListaAuditorio * auditorio;

    std::list< InterfaceMensagem * > lista;

    Protocolo protocolo;

};

#endif //NOTEPADSERVIDOR_THREADCLIENTE_H
