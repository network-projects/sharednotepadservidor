//
// Created by debianstnin on 07/10/18.
//

#ifndef NOTEPADSERVIDOR_INTERFACELISTA_H
#define NOTEPADSERVIDOR_INTERFACELISTA_H

#include <list>
#include "../Cliente/InterfaceMensagem.h"
#include "../Cliente/InterfaceCliente.h"

class InterfaceLista{

public:

    virtual std::list< InterfaceMensagem * > obterListaNomeClientesString() = 0;
    virtual void remover( InterfaceCliente * cliente ) = 0;

};

#endif //NOTEPADSERVIDOR_INTERFACELISTA_H
