//
// Created by debianstnin on 07/10/18.
//

#ifndef NOTEPADSERVIDOR_INTERFACELISTAAUDITORIO_H
#define NOTEPADSERVIDOR_INTERFACELISTAAUDITORIO_H

#include "../Cliente/InterfaceCliente.h"
#include "InterfaceLista.h"

class InterfaceListaAuditorio : public InterfaceLista{

public:

    virtual void inserir( InterfaceCliente * cliente ) = 0;
    virtual void enviarParaClientes( InterfaceMensagem * mensagem ) = 0;

};

#endif //NOTEPADSERVIDOR_INTERFACELISTAAUDITORIO_H
