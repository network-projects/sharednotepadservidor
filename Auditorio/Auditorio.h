//
// Created by debianstnin on 07/10/18.
//

#ifndef NOTEPADSERVIDOR_AUDITORIO_H
#define NOTEPADSERVIDOR_AUDITORIO_H

#include <iostream>
#include <map>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <mutex>

#include "../Cliente/InterfaceClienteComunicacao.h"
#include "InterfaceListaAuditorio.h"

#define ZERO 0

class Auditorio : public InterfaceListaAuditorio{

public:

    explicit Auditorio();

    bool salaVazia();
    bool salaCheia();

    void enviarParaClientes(InterfaceMensagem *  mensagem ) override;
    void inserir( InterfaceCliente * cliente ) override;

    std::list< InterfaceMensagem * > obterListaNomeClientesString() override;
    void remover( InterfaceCliente * cliente ) override;

private:

    InterfaceClienteComunicacao * orador;

    bool cheia;
    bool vazia;

    const int LIMITE_MAX_CLIENTES = 30;

    std::map< int, InterfaceCliente * > mapa;

    std::mutex protecao;

};

#endif //NOTEPADSERVIDOR_AUDITORIO_H
