//
// Created by debianstnin on 07/10/18.
//

#include "Auditorio.h"

Auditorio::Auditorio() {

    orador = nullptr;

    cheia = false;
    vazia = true;

}

bool Auditorio::salaVazia() {

    return vazia;

}

bool Auditorio::salaCheia() {

    return cheia;

}

void Auditorio::enviarParaClientes(InterfaceMensagem * mensagem) {

    if( mensagem != nullptr ) {

        for (auto it:mapa) {

            if (it.first != orador->obterIdentificador()->paraInteiro()) {

                std::lock_guard<std::mutex> lock(protecao);

                auto cliente = it.second;

                cliente->enviarMensagem(mensagem);

                std::cout << "Mensagem enviada para Cliente " << cliente << std::endl;

            }

        }

    }

}

void Auditorio::inserir(InterfaceCliente * cliente) {

    if( mapa.size() < LIMITE_MAX_CLIENTES ){

        std::lock_guard<std::mutex> lock(protecao);

        cheia = false;
        mapa.insert( std::pair<int, InterfaceCliente * >( (cliente->obterIdentificador()->paraInteiro()), cliente) );

        std::cout << "Cliente " << cliente->obterIdentificador()->paraInteiro() << " Inserido na Lista" << std::endl;

    }else {

        cheia = true;

    }

}

std::list<InterfaceMensagem *> Auditorio::obterListaNomeClientesString() {

    std::list<InterfaceMensagem *> listaClientes;

    for( auto it:mapa ){

        std::lock_guard<std::mutex> lock(protecao);

        listaClientes.push_back( it.second->obterNomeCliente() );

    }

    return listaClientes;
}

void Auditorio::remover(InterfaceCliente * cliente) {

    if( !mapa.empty() ) {

        std::lock_guard<std::mutex> lock(protecao);

        vazia = false;
        mapa.erase(cliente->obterIdentificador()->paraInteiro());

        std::cout << "Cliente " << cliente->obterIdentificador()->paraInteiro() << " Removido da Lista" << std::endl;

    }else {

        vazia = true;

    }
}

