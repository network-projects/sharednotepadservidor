//
// Created by debianstnin on 07/10/18.
//

#ifndef NOTEPADSERVIDOR_CLIENTE_H
#define NOTEPADSERVIDOR_CLIENTE_H

#include "InterfaceClienteComunicacao.h"

class Cliente : public InterfaceClienteComunicacao{

public:

    InterfaceMensagem * obterNomeCliente() override;

    void definirNomeCliente( InterfaceMensagem * nome ) override;

private:

    InterfaceMensagem * nome;

};

#endif //NOTEPADSERVIDOR_CLIENTE_H
