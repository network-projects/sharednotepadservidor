//
// Created by debianstnin on 07/10/18.
//

#ifndef NOTEPADSERVIDOR_INTERFACECLIENTECOMUNICACAO_H
#define NOTEPADSERVIDOR_INTERFACECLIENTECOMUNICACAO_H

#include "InterfaceMensagem.h"
#include "InterfaceCliente.h"

class InterfaceClienteComunicacao : public InterfaceCliente{

public:

    virtual ~InterfaceClienteComunicacao() = default;
    virtual void definirNomeCliente( InterfaceMensagem * nome ) = 0;
    virtual InterfaceMensagem * receberMensagem() = 0;
    virtual bool mensagemRecebidaSucesso() = 0;

};

#endif //NOTEPADSERVIDOR_INTERFACECLIENTECOMUNICACAO_H
