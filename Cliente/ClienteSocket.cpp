//
// Created by debianstnin on 07/10/18.
//
#include "ClienteSocket.h"


ClienteSocket::ClienteSocket(InterfaceIdentificador * descritorCliente) {

    std::cout << "Cliente " << descritorCliente->paraInteiro() << " Conectado" << std::endl;

    this->descritor = descritorCliente;

    conectado = true;
    msgEnviadaSucesso = false;
    msgRecebidaSucesso = false;

}

ClienteSocket::~ClienteSocket() {

    conectado = false;

    close( descritor->paraInteiro() );

    std::cout << "Cliente " << descritor->paraInteiro() << " Desconectado" << std::endl;

}

void ClienteSocket::enviarMensagem(InterfaceMensagem * mensagem) {

    auto msg = mensagem->paraString();

    ssize_t size = write(descritor->paraInteiro(), msg.data() , msg.size() );

}

InterfaceMensagem * ClienteSocket::receberMensagem() {

    char msg[ TAMANHO_BUFFER ];

    ssize_t size = read(descritor->paraInteiro(), msg , TAMANHO_BUFFER );

    msg[size] = '\0';

    return new Mensagem( msg );
}

void ClienteSocket::statusEnviar(ssize_t size) {

    conectado = size != ZERO;
    msgEnviadaSucesso = size > ZERO;

}

void ClienteSocket::statusReceber(ssize_t size) {

    conectado = size != ZERO;
    msgRecebidaSucesso = size > ZERO;

}

bool ClienteSocket::clienteConectado() {

    return conectado and descritor->paraInteiro() > ZERO;

}

bool ClienteSocket::mensagemEnviadaSucesso() {

    return msgEnviadaSucesso;

}

bool ClienteSocket::mensagemRecebidaSucesso() {

    return msgRecebidaSucesso;

}

InterfaceIdentificador * ClienteSocket::obterIdentificador() {

    return descritor;

}







