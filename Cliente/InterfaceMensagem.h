//
// Created by debianstnin on 07/10/18.
//

#ifndef NOTEPADSERVIDOR_INTERFACEMENSAGEM_H
#define NOTEPADSERVIDOR_INTERFACEMENSAGEM_H

#include <string>

class InterfaceMensagem{

public:

    virtual ~InterfaceMensagem() = default;
    virtual std::string paraString() = 0;

};

#endif //NOTEPADSERVIDOR_INTERFACEMENSAGEM_H
