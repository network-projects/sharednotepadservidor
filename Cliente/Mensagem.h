#include <utility>

//
// Created by debianstnin on 07/10/18.
//

#ifndef NOTEPADSERVIDOR_MENSAGEM_H
#define NOTEPADSERVIDOR_MENSAGEM_H

#include <string>
#include "InterfaceMensagem.h"

class Mensagem : public InterfaceMensagem{

public:

    explicit Mensagem( std::string mensagem ) : mensagem(std::move(mensagem)) {}

    std::string paraString() override {

        return mensagem;

    }

private:

    std::string mensagem;

};

#endif //NOTEPADSERVIDOR_MENSAGEM_H
