//
// Created by debianstnin on 07/10/18.
//

#ifndef NOTEPADSERVIDOR_CLIENTESOCKET_H
#define NOTEPADSERVIDOR_CLIENTESOCKET_H

#include <unistd.h>
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>

#include "../Servidor/InterfaceIdentificador.h"
#include "InterfaceMensagem.h"
#include "Cliente.h"
#include "Mensagem.h"

#define ZERO 0

class ClienteSocket : public Cliente{

public:

    explicit ClienteSocket( InterfaceIdentificador * descritorCliente );
    ~ClienteSocket();

    void enviarMensagem( InterfaceMensagem * mensagem ) override;
    InterfaceMensagem * receberMensagem() override;

    bool clienteConectado() override;
    bool mensagemEnviadaSucesso() override;
    bool mensagemRecebidaSucesso() override;

    InterfaceIdentificador * obterIdentificador() override;

private:

    InterfaceIdentificador * descritor;

    bool conectado;
    bool msgEnviadaSucesso;
    bool msgRecebidaSucesso;

    const int TAMANHO_BUFFER = 4096;

    void statusEnviar( ssize_t size );
    void statusReceber( ssize_t size );


};

#endif //NOTEPADSERVIDOR_CLIENTESOCKET_H
