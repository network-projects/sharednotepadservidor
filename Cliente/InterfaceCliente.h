//
// Created by debianstnin on 07/10/18.
//

#ifndef NOTEPADSERVIDOR_INTERFACECLIENTE_H
#define NOTEPADSERVIDOR_INTERFACECLIENTE_H

#include "InterfaceMensagem.h"
#include "../Servidor/InterfaceIdentificador.h"

class InterfaceCliente{

public:

    virtual InterfaceMensagem * obterNomeCliente() = 0;
    virtual void enviarMensagem( InterfaceMensagem * mensagem ) = 0;
    virtual bool clienteConectado() = 0;
    virtual bool mensagemEnviadaSucesso() = 0;
    virtual InterfaceIdentificador * obterIdentificador() = 0;

};

#endif //NOTEPADSERVIDOR_INTERFACECLIENTE_H
