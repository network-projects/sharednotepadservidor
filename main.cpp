#include <iostream>
#include "Servidor/Porta.h"
#include "Servidor/ServidorSocket.h"
#include "Auditorio/Auditorio.h"
#include "Threads/ThreadCliente.h"
#include "Semaforo/Semaforo.h"

void conexoes( ServidorSocket servidor );

int main() {

    Porta porta( 2018 );
    ServidorSocket servidor( porta );
    Semaforo semaforo;

    servidor.inicializarServidor();

    if ( servidor.servidorConectado() ){

        conexoes( servidor );

    }

    return 0;
}

void conexoes( ServidorSocket servidor ){

    Auditorio auditorio;

    while( servidor.servidorConectado() ) {

        auto cliente = servidor.aceitarClientes();

        if( cliente->clienteConectado() ) {

            auditorio.inserir(cliente);

            std::thread(&ThreadCliente::definirTipoCliente, move(std::make_unique<ThreadCliente>(cliente, &auditorio))).detach();

        }

    }

}