//
// Created by debianstnin on 08/10/18.
//

#ifndef NOTEPADSERVIDOR_SEMAFORO_H
#define NOTEPADSERVIDOR_SEMAFORO_H

#include "../Cliente/InterfaceCliente.h"
#include "../Servidor/Token.h"

class Semaforo {

public:

    explicit Semaforo();

    void liberarEscritaInicial();
    void liberarEscrita();
    void bloquearEscrita();


private:

    InterfaceCliente * orador;

};


#endif //NOTEPADSERVIDOR_SEMAFORO_H
