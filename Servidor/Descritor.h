//
// Created by debianstnin on 07/10/18.
//

#ifndef NOTEPADSERVIDOR_DESCRITOR_H
#define NOTEPADSERVIDOR_DESCRITOR_H

#include "InterfaceIdentificador.h"

class Descritor : public InterfaceIdentificador{ // Descritor <Class>

public:

    explicit Descritor( int descritor ) : descritor(descritor) {}  // Descritor() :: Comportamento {Construtor}


    // override :: Interface Identificador
    int paraInteiro() override {   // paraInteiro() : unsigned int :: Comportamento {Retorna a descritor}

        return this->descritor;

    }

private:

    int descritor;   // descritor : const unsigned int :: Atributo

};

#endif //NOTEPADSERVIDOR_DESCRITOR_H
