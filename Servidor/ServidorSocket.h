//
// Created by debianstnin on 07/10/18.
//

#ifndef NOTEPADSERVIDOR_SERVIDORSOCKET_H
#define NOTEPADSERVIDOR_SERVIDORSOCKET_H

#include <iostream>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <cstring>

#include "Porta.h"
#include "Token.h"
#include "InterfaceIdentificador.h"

#include "../Cliente/InterfaceClienteComunicacao.h"
#include "../Cliente/ClienteSocket.h"
#include "Descritor.h"

#define ZERO 0

class ServidorSocket{

public:

    explicit ServidorSocket( Porta porta );
    ~ServidorSocket();

    void inicializarServidor();

    bool servidorConectado();

    InterfaceClienteComunicacao * aceitarClientes();

    // TODO : voltar Token

private:

    Porta porta;
    Descritor descritor;

    const Token token;
    const unsigned int LIMITE_MAX_CLIENTES = 30;

    struct sockaddr_in enderecoLocal;

    bool conectado;

};

#endif //NOTEPADSERVIDOR_SERVIDORSOCKET_H
