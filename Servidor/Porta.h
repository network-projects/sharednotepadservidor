//
// Created by debianstnin on 07/10/18.
//

#ifndef NOTEPADSERVIDOR_PORTA_H
#define NOTEPADSERVIDOR_PORTA_H

#include <netinet/in.h>

class Porta{ // Porta <Class>

public:

    explicit Porta(const in_port_t porta) : porta(porta) {}
    // Porta() :: Comportamento {Construtor}

    in_port_t paraInteiro() const {   // paraInteiro() : unsigned int :: Comportamento {Retorna a porta}

        return this->porta;

    }

private:

    const in_port_t porta;   // porta : const unsigned int :: Atributo

};

#endif //NOTEPADSERVIDOR_PORTA_H
