//
// Created by debianstnin on 07/10/18.
//

#ifndef NOTEPADSERVIDOR_TOKEN_H
#define NOTEPADSERVIDOR_TOKEN_H

#include <cstdlib>
#include <ctime>
#include <iostream>
#include <random>

class Token{ // Token <Class>

public:

    explicit Token(  ){  // Token() :: Comportamento {Construtor}

        std::random_device random;
        std::mt19937 gerarAleatorio( random() );
        std::uniform_int_distribution<> distribuicao( 1000, 4000 );

        token = static_cast<unsigned int>(distribuicao(gerarAleatorio));

    }

    unsigned int paraInteiro() {   // gerarToken() : unsigned int :: Comportamento {Retorna um token aleatorio}

        return this->token;

    }

private:

    unsigned int token;   // token : const unsigned int :: Atributo

};

#endif //NOTEPADSERVIDOR_TOKEN_H
