#include "ServidorSocket.h"

ServidorSocket::ServidorSocket( Porta porta ) : porta(porta), descritor( socket(AF_INET, SOCK_STREAM, ZERO) ) {

    conectado = false;

    std::memset(&enderecoLocal, 0x00, sizeof(enderecoLocal));

    enderecoLocal.sin_family = AF_INET;                             // configuraçao - AF_INET = Formato IPV4
    enderecoLocal.sin_port = htons( this->porta.paraInteiro() );    // configuraçao - PORTA
    enderecoLocal.sin_addr.s_addr = INADDR_ANY;                     // configuraçao - Escolha de enrdereço local

}

ServidorSocket::~ServidorSocket(){

    close( descritor.paraInteiro() );  // fecha o socket

    std::cout << "Fechando Servidor" << std::endl;

}

void ServidorSocket::inicializarServidor(){

    std::cout << "Inicializando Servidor" << std::endl;

    conectado = bind(descritor.paraInteiro(), (struct sockaddr *) &this->enderecoLocal, sizeof(this->enderecoLocal)) >= ZERO and
                listen(descritor.paraInteiro(), LIMITE_MAX_CLIENTES) >= ZERO;

}

bool ServidorSocket::servidorConectado(){

    return conectado;

}

InterfaceClienteComunicacao * ServidorSocket::aceitarClientes(){

    struct sockaddr_in enderecoCliente{};
    socklen_t tamanhoEnderecoCliente = sizeof(enderecoCliente);

    auto descritorCliente = new Descritor( accept(descritor.paraInteiro(), (struct sockaddr *) &enderecoCliente, &tamanhoEnderecoCliente) );

    return new ClienteSocket( descritorCliente );

    // aceita cliente

}