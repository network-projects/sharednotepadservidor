//
// Created by debianstnin on 07/10/18.
//

#ifndef NOTEPADSERVIDOR_INTERFACEIDENTIFICADOR_H
#define NOTEPADSERVIDOR_INTERFACEIDENTIFICADOR_H

class InterfaceIdentificador{

    public:

        virtual int paraInteiro() = 0;

};

#endif //NOTEPADSERVIDOR_INTERFACEIDENTIFICADOR_H
